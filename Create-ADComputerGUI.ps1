﻿Import-Module ActiveDirectory

Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

# Create the main form
$form = New-Object Windows.Forms.Form
$form.Text = "Create AD Computer. Example OU Path: OU=TestComputers,OU=DukeDepts,DC=win,DC=duke,DC=edu"
$form.Size = New-Object Drawing.Size(800, 200)

# Create labels
$label1 = New-Object Windows.Forms.Label
$label1.Text = "Computer Name:"
$label1.Location = New-Object Drawing.Point(10, 20)
$label1.AutoSize = $true

$label2 = New-Object Windows.Forms.Label
$label2.Text = "OU Path:"
$label2.Location = New-Object Drawing.Point(10, 50)
$label2.AutoSize = $true

# Create textboxes
$textBox1 = New-Object Windows.Forms.TextBox
$textBox1.Location = New-Object Drawing.Point(120, 20)
$textBox1.Size = New-Object Drawing.Size(200, 20)

$textBox2 = New-Object Windows.Forms.TextBox
$textBox2.Location = New-Object Drawing.Point(120, 50)
$textBox2.Size = New-Object Drawing.Size(200, 20)
$textBox2.Text = ",DC=win,DC=duke,DC=edu"

# Create a button
$button = New-Object Windows.Forms.Button
$button.Text = "Create"
$button.Location = New-Object Drawing.Point(10, 90)
$button.Add_Click({
    $computerName = $textBox1.Text
    $ouPath = $textBox2.Text

    # Create the computer object in Active Directory
    try {
        #$newComputer = New-Object DirectoryServices.DirectoryEntry("LDAP://$ouPath")
        #$newComputer.Invoke("Create", "Computer", "CN=$computerName")
        #$newComputer.CommitChanges()
        #$newComputer.Close()
        New-ADComputer -Name $computerName -SamAccountName $computerName -Path $oupath -Enabled $True
        [System.Windows.Forms.MessageBox]::Show("Computer object created successfully.", "Success", [System.Windows.Forms.MessageBoxButtons]::OK, [System.Windows.Forms.MessageBoxIcon]::Information)
    }
    catch {
        [System.Windows.Forms.MessageBox]::Show("Error creating computer object: $_", "Error", [System.Windows.Forms.MessageBoxButtons]::OK, [System.Windows.Forms.MessageBoxIcon]::Error)
    }
})

# Add controls to the form
$form.Controls.Add($label1)
$form.Controls.Add($label2)
$form.Controls.Add($textBox1)
$form.Controls.Add($textBox2)
$form.Controls.Add($button)

# Show the form
$form.ShowDialog()
